# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.5.0] - 2018-8-26
### Added
- BiblioCraft
- Ender Storage
- Decocraft
- Mr. Crayfish Furniture
- Wearable Backpacks
- Twilight Forest
- Extra Utilities
- Open Blocks
- OpenModsLib
- Wizardry
- LibrarianLib
- Shadowfact's Forelin
- Erebus
- The Betweenlands
- PTRLib

## [0.4.0] - 2018-8-25
### Added
- Dockerfile for server
- Forge Server
- Lang Switch for server
- Pieconomy for server
- Sponge Forge for server
- Villager Shops for server

- Tinkers Construct
- Immersive Engineering
- Applied Energistics
- RF Tools
- McJtyLib
- EnderIO
- EnderCore
- RF Tools Control
- Compact Machines
- Mekanism
- OpenComputers
- RF Tools Dimensions
- Advanced Rocketry
- JurassiCraft
- SGCraft
- Chisels n' Bits
- Storage Drawers
- LibVulpes
- Chameleon
- Mantle
- Llibrary
- Ancient Warfare
- CodeChickenLib

### Changed
- Msi has embed cab
- Updated README.md

## [0.3.0] - 2018-8-24
### Added
- AbyssalCraft
- AbyssalCraftIntegrations
- Astral Sorcery
- AutoRegLib
- Baubles
- Blood Magic
- Botania
- Cucumber
- Embers
- GuideAPI
- MystCraft
- Mystical Agradditions
- Mystical Agriculture
- Psi
- Roots
- Thaumcraft

### Changed
- Updated README.md

## [0.2.0] - 2018-8-24
### Added
- Apple Core
- Biome Bundle
- Chickens
- JEI
- More Chickens
- Open Terrain Generator
- Pam's HarvestCraft
- Quick Leaf Decay
- Rogue like Dungeons
- Roost
- Serene Seasons
- Spice of Life

### Changed
- Updated README.md

## [0.1.0] - 2018-8-24
### Added
- Base installer to install Minecraft Forge.