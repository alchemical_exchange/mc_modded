FROM ubuntu

RUN apt-get update; \
    apt-get install -y openjdk-8-jre

RUN mkdir /mnt/files

# Libraries
COPY server/ /mnt/files/
COPY libraries/ /mnt/files/libraries/
COPY server_libraries/ /mnt/files/libraries/
# Mods
COPY mods/ /mnt/files/mods/
COPY server_mods/ /mnt/files/mods/
# Config
COPY config/ /mnt/files/config/
COPY server_config/ /mnt/files/config/

COPY start_container.sh /opt/
RUN chmod 744 /opt/start_container.sh
EXPOSE 25565
ENTRYPOINT /opt/start_container.sh
