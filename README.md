# Alchemical Exchange Mod Pack

## Minecraft Version
- 1.12.2

## Included Mods
##### Minecraft Forge
- AbyssalCraft (Shinoow)
- AbyssalCraftIntegrations (Shinoow)
- Astral Sorcery (HellFirePvP)
- Baubles (Azanor)
- Biome Bundle (MC_Pitman)
- Blood Magic (WayofTime)
- Botania (Vazkii)
- Chickens (Setycz, GenDeathrow, LordCazsius)
- Embers
- JEI
- More Chickens
- MystCraft
- Mystical Agradditions
- Mystical Agriculture
- Open Terrain Generator (Team OTG)
- Pam's HarvestCraft
- Psi
- Quick Leaf Decay
- Rogue like Dungeons
- Roost
- Roots
- Thaumcraft
- Serene Seasons
- Spice of Life












